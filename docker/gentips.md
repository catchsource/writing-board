#H General Commands.
1. Docker commands.
 1. | Command | Purpose | example |
    | --------|:-------:|--------:|
    | image ls| list all the image | docker image ls|
    | run -it | run the image that is present in docker image list| docker run -it centos7r1|
    | pull image| It pull the image from docker image repository | docker pull bitnami/nginx:latest or bitnami/nginx:[tag] |
2. nginx commands.


